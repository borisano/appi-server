-- MySQL dump 10.13  Distrib 5.5.34, for debian-linux-gnu (x86_64)
--
-- Host: localhost    Database: appi
-- ------------------------------------------------------
-- Server version	5.5.34-0ubuntu0.13.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `User`
--

DROP TABLE IF EXISTS `User`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `User` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `login` varchar(45) NOT NULL,
  `pass` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `login_UNIQUE` (`login`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `User`
--

LOCK TABLES `User` WRITE;
/*!40000 ALTER TABLE `User` DISABLE KEYS */;
INSERT INTO `User` VALUES (1,'borisano','1111'),(2,'lotter','1111'),(3,'new user','new pass'),(5,'new userhh','new pass'),(6,'borisanos','2222'),(7,'borisadno','11112'),(8,'a','b'),(9,'aa','b'),(10,'newLogin','newPassword'),(11,'afaf','3232'),(12,'a1212','222'),(13,'bbb','aaa'),(14,'sefselsefkjslefkjsl;afjnba','timmydd'),(15,'boris','ano');
/*!40000 ALTER TABLE `User` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `UserPosition`
--

DROP TABLE IF EXISTS `UserPosition`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `UserPosition` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `session_id` int(11) NOT NULL,
  `x` double NOT NULL,
  `y` double NOT NULL,
  `report_time` datetime NOT NULL,
  `stop` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `fk_UserPosition_1_idx` (`user_id`),
  CONSTRAINT `fk_UserPosition_1` FOREIGN KEY (`user_id`) REFERENCES `User` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=47 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `UserPosition`
--

LOCK TABLES `UserPosition` WRITE;
/*!40000 ALTER TABLE `UserPosition` DISABLE KEYS */;
INSERT INTO `UserPosition` VALUES (3,1,0,49.2317,53.2123,'0000-00-00 00:00:00',0),(4,1,0,49.2317,53.2123,'0000-00-00 00:00:00',0),(5,1,0,49.2317,53.2123,'2013-12-15 17:36:30',0),(6,1,0,49.2317,53.2123,'2013-12-15 17:44:58',0),(7,1,0,0,49.2317,'2013-12-15 17:49:19',0),(8,1,0,49.2317,53.2123,'2013-12-15 17:50:01',0),(9,1,13,49.2317,53.2123,'2013-12-15 17:50:31',0),(10,1,13,49.2317,53.2123,'2013-12-15 18:00:08',0),(11,1,13,49.2317,53.2123,'2013-12-15 18:00:26',0),(12,1,13,49.2317,53.2123,'2013-12-15 18:07:39',0),(13,1,13,49.2317,53.2123,'2013-12-15 18:08:47',0),(14,1,13,49.2317,53.2123,'2013-12-15 18:08:55',0),(15,1,14,49.2317,53.2123,'2013-12-15 18:11:13',0),(16,1,14,49.2317,53.2123,'2013-12-15 19:51:38',0),(17,1,17,49.2317,53.2122,'2013-12-15 20:23:18',0),(18,1,17,50,60,'2013-12-16 23:13:06',0),(19,1,17,12,12,'2013-12-16 23:14:05',0),(20,1,17,44,44,'2013-12-16 23:14:05',0),(21,1,17,44,22,'2013-12-16 23:14:05',0),(22,13,28,22,33,'2013-12-17 01:15:47',0),(23,13,28,22,33,'2013-12-17 01:15:53',0),(24,14,29,22,33,'2013-12-17 01:28:34',0),(25,14,29,22,234.2,'2013-12-17 01:28:42',0),(26,14,29,22,234.2,'2013-12-17 01:28:44',0),(27,14,29,222,234.2,'2013-12-17 01:28:49',0),(28,13,28,22,33,'2013-12-17 17:15:52',0),(29,15,30,22,33,'2013-12-17 17:16:02',0),(30,15,30,22,33,'2013-12-17 17:16:09',1),(31,15,30,22,33,'2013-12-17 17:16:10',0),(32,15,30,25,30,'2013-12-17 17:16:23',1),(33,15,30,25,35,'2013-12-17 17:16:25',1),(34,15,30,28,38,'2013-12-17 17:16:34',0),(35,15,30,28,40,'2013-12-17 17:16:40',1),(36,15,30,28,41,'2013-12-17 17:16:42',0),(37,15,30,31,42,'2013-12-17 17:16:51',1),(38,15,30,31,49,'2013-12-17 17:16:54',0),(39,15,30,31,50,'2013-12-17 17:16:59',0),(40,15,30,35,50,'2013-12-17 17:17:06',0),(41,15,30,38,50,'2013-12-17 17:17:09',0),(42,15,30,40,50,'2013-12-17 17:17:57',1),(43,15,30,40,53,'2013-12-17 17:18:00',1),(44,15,30,40,53,'2013-12-17 17:18:03',1),(45,15,30,41.5,53,'2013-12-17 17:18:14',0),(46,15,30,41.5,60,'2013-12-17 17:18:21',1);
/*!40000 ALTER TABLE `UserPosition` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `UserSession`
--

DROP TABLE IF EXISTS `UserSession`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `UserSession` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `session_hash` varchar(45) NOT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `fk_UserSession_1_idx` (`user_id`),
  CONSTRAINT `fk_UserSession_1` FOREIGN KEY (`user_id`) REFERENCES `User` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `UserSession`
--

LOCK TABLES `UserSession` WRITE;
/*!40000 ALTER TABLE `UserSession` DISABLE KEYS */;
INSERT INTO `UserSession` VALUES (1,1,'1Tx6FTIowDnWEJf',0),(2,1,'KHuepIP5iVQEIDq',0),(3,1,'jMf7ZpIbjqODPgj',0),(4,1,'mhWktceM0VX4nhw',0),(5,1,'tnyKqHWQ8ihoEvp',0),(6,1,'m05vrzwwdG0sxEz',0),(7,1,'Urt1vNWduaBe7Zb',0),(8,8,'OTeoalEQCz721H8',0),(9,1,'pZqbgf1rZJHp70J',0),(10,1,'2nArEuDesSY5pxC',0),(11,8,'krJICyi1HChhUdk',0),(12,1,'KjddzMtoabcSbqt',0),(13,1,'iLczqqNFQSsfP6Q',0),(14,1,'Yiw3NTowcZ6S7aN',0),(15,1,'dy9LcutLlbuQ9gE',0),(16,1,'cWcPVGTGKvhlQxb',0),(17,1,'bdbA916A7knT3Zc',1),(18,8,'abqhLqRDagCVXP4',1),(19,9,'i05lGkBr2sNssFS',0),(20,9,'tPiDYvHaiEQsbEK',1),(21,10,'H5WodgKgOOcs22X',0),(22,10,'QfOrI6FI9rgnoAV',0),(23,10,'JtoWlI7qEMUgyRD',0),(24,10,'7OVy5HZXkaeX5bM',1),(25,11,'mMwNm2mo8DZmPrb',1),(26,12,'aFCtjAY1LgFCJQg',0),(27,12,'FVlw0VR6jKhNl7d',1),(28,13,'wPitdwKXNyMZ0O1',1),(29,14,'5irTJ90pS9Q9wfK',1),(30,15,'n891nHnaV5k8L5x',1);
/*!40000 ALTER TABLE `UserSession` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Watcher`
--

DROP TABLE IF EXISTS `Watcher`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Watcher` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `login` varchar(45) NOT NULL,
  `pass` varchar(45) NOT NULL,
  `phone` varchar(45) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `login_UNIQUE` (`login`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Watcher`
--

LOCK TABLES `Watcher` WRITE;
/*!40000 ALTER TABLE `Watcher` DISABLE KEYS */;
/*!40000 ALTER TABLE `Watcher` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2013-12-17 17:19:48
