<?php

class Application_Model_DbTable_Users extends Zend_Db_Table_Abstract
{
    protected $_name = 'User';

    protected $_primary = 'id';

    public static function login( $login, $pass )  {
        $usersModel = new self();
     
        $user = self::findByLogin($login);
        
        $loginResults = array();
        $loginResults['success'] = false;
        
        // check if user exists
        if( !$user ) {
            $loginResults['message'] = 'Successfull registration';
            
            $user = $usersModel->createUser( $login, $pass );
        }
        else { // if user exists - check password
            $loginResults['message'] = 'Successfull login';
            $isCorrectPass = self::checkPassword( $user->id, $pass );
            
            if( ! $isCorrectPass ) {
                // TODO send correct message
                $loginResults['success'] = false;
                $loginResults['message'] = 'Wrong password';
                
                return $loginResults;
            }
        }
        // create session for user
        $session = Application_Model_DbTable_UserSessions::openSessionForUser( $user->id );

        $loginResults['sessionHash'] = $session->session_hash;
        $loginResults['success'] = true;
        
        return $loginResults;
    }     
    
    public static function findByLogin( $login ) {
        $users = new self();
        $user = $users->fetchRow(
                    $users->select()
                    ->where('login = ?', $login)
                );

        return $user;
    }
    
    // 
    public static function checkPassword( $userID, $passToCheck) {
        $usersModel = new self();
        
        $user = $usersModel->find( $userID )->current();
        
        if(!$user ) {
            return false;
        }

        return $user->pass == $passToCheck;
    }
    
    public static function createUser( $login, $pass ) {
        $users = new self();
        
        $user = $users->createRow(array(
            'login' => $login,
            'pass'  => $pass
        ));
        
        $user->save();
        
        return $user;
    }
    
    public static function getUsersInfo() {
        $usersModel = new self();              
        $result = array();
        
        $users = $usersModel->fetchAll();
        
        foreach($users as $u) {
            $result[] = array(
                $u->id, $u->login
            );
        }

        return $result;
        
    }
}