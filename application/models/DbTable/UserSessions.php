<?php

class Application_Model_DbTable_UserSessions extends Zend_Db_Table_Abstract
{
    protected $_name = 'UserSession';

    protected $_primary = 'id';
    
    public static function openSessionForUser( $userID ) {
        $userSessionsModel = new self();

        // close all previous sessions of user
        $userSessions = $userSessionsModel->fetchAll(
                $userSessionsModel->select()
                ->where('user_id = ?', $userID)
                ->where('active = ?', 1 )
        ); 
        foreach( $userSessions as $s ) {
            $s->active = 0;
            $s->save();
        }
        
        $newSession = $userSessionsModel->createRow(array(
            'user_id' => (int) $userID,
            'session_hash' => self::generateRandomString(),
            'active'  => 1
        ));
        $newSession->save();
        
        return $newSession;
    }
    
    public static function isEmptySession( $sessionHash ) {
        $sessionID = self::getSessionIDByHash( $sessionHash );
        
        $route = Application_Model_DbTable_UserPositions::getSessionPositions( $sessionID );
        
        return count($route) == 0;
    }
    
    public static function getUserIDOfSession( $sessionHash ) {
        $userSessionsModel = new self();

        $userSession = $userSessionsModel->fetchAll(
            $userSessionsModel->select()
            ->where('session_hash = ?', $sessionHash)
        )->current(); 

        if( ! $userSession ) {
            return false;
        }
        
        return $userSession->user_id;
    }
    
    public static function getSessionIDByHash( $sessionHash ) {
        $userSessionsModel = new self();
        
        $userSession = $userSessionsModel->fetchAll(
            $userSessionsModel->select()
            ->where('session_hash = ?', $sessionHash)
        )->current(); 
        
        if( !$userSession ) {
            return false;
        }
        
        return $userSession->id;
        
    }
    
    public static function getMostRecentSessionOfUser( $userID ) {
        $userSessionsModel = new self();

        $userSession = $userSessionsModel->fetchAll(
            $userSessionsModel->select()
            ->where('user_id = ?', $userID)
            ->order('id DESC')
            ->limit(1,0)
        )->current(); 
        
        if( !$userSession ) {
            return 0;
        }
        
        return $userSession->id;
    }
    
    // check if session belongs to user and is active
    public static function isActiveSessionOfUser( $userID, $sessionHash ) {
        $activeSession = self::getActiveSessionOfUser($userID);

        return $activeSession->session_hash == $sessionHash;
    }
    
    public static function getActiveSessionOfUser( $userID ) {
        $userSessionsModel = new self();

        $userSession = $userSessionsModel->fetchAll(
            $userSessionsModel->select()
            ->where('user_id = ?', $userID)
            ->where('active = ?', 1 )
        )->current(); 
        
        if( ! $userSession ) {
            return false;
        }
        
        return $userSession;
    }
    
    private static function generateRandomString($length = 15) {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, strlen($characters) - 1)];
        }
        return $randomString;
    }  

}