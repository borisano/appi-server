<?php

class Application_Model_DbTable_UserPositions extends Zend_Db_Table_Abstract
{
    protected $_name = 'UserPosition';

    protected $_primary = 'id';
    
    public static function saveUserPosition( $userID, $x, $y, $stop) {        
        $positionsModel = new self();
        
        $position = $positionsModel->createRow(array(
            'user_id'       => $userID,
            'session_id'    => Application_Model_DbTable_UserSessions::getActiveSessionOfUser( $userID )->id,
            'x'             => $x,
            'y'             => $y,
            'report_time'   => new Zend_Db_Expr('NOW()'),
            'stop'          => $stop ? 1 : 0
        ));
        
        $position->save();
        
        return true;
    }
    
    public static function getLastUserRoute( $userID ) {
       // get last user sessio
       $sessionID = Application_Model_DbTable_UserSessions::getMostRecentSessionOfUser( $userID );

        $userPositionsModel = new self();

        $route = $userPositionsModel->fetchAll(
            $userPositionsModel->select()
            ->where('session_id = ?', $sessionID)
            ->order('id ASC')
        ); 
        
        if( !$route ) {
            return false;
        }
        
        $result = array();
        foreach( $route as $point )
        {
           $result[] = array(
               'x'      => $point->x,
               'y'      =>$point->y,
               'stop'      =>$point->stop
           );
        }

        return $result;
    }
    
    public static function getSessionPositions( $sessionID ) {
        $userPositionsModel = new self();

        $route = $userPositionsModel->fetchAll(
            $userPositionsModel->select()
            ->where('session_id = ?', $sessionID)
        ); 
        
        if( !$route ) {
            return false;
        }
        
        return $route;
    }

}