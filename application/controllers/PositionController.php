<?php

class PositionController extends Zend_Controller_Action
{

    public function init()
    {

    }

    public function indexAction()
    {
        $response = array(
            'action'    => 'position'
        );
        
        try {
            // check parameters
            $params = $this->getRequest()->getParams();

            $sessionHash = htmlspecialchars( $params['sessionHash'] );
            $x = (double) $params['x'];
            $y = (double) $params['y'];
            $isStop = (bool) $params['stop'];
            
            $userID = Application_Model_DbTable_UserSessions::getUserIDOfSession( $sessionHash );

            // check if session is correct
            $isSessionCorrect = Application_Model_DbTable_UserSessions::isActiveSessionOfUser( $userID, $sessionHash);
            if( !$isSessionCorrect ) {
                throw new Exception('Session is wrong');
            }
            
            $isEmptySession = Application_Model_DbTable_UserSessions::isEmptySession( $sessionHash );

            // write to DB
            $savingResult = Application_Model_DbTable_UserPositions::saveUserPosition($userID, $x, $y, $isStop);

            if( !$savingResult ) {
                throw new Exception( 'Error while position saving' );
            }
            
            //if it's first position in session - return also watcher's phone
            $phone = '+48794619026';
            if( $isEmptySession ) {
                $response['watcherPhone'] = $phone;
            }
            
            $response['success'] = true;
            $response['message'] = 'Position successfully saved';

        } catch (Exception $e) {
            $response['success'] = false;
            $response['message'] = $e->getMessage();
            
        }
        
        header('Content-type: application/json');
        echo json_encode( $response );die;
        
    }

}

