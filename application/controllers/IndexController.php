<?php

class IndexController extends Zend_Controller_Action
{

    public function init()
    {
//        $this->_helper->viewRenderer->setNoRender(true);
//        $this->_helper->layout->disableLayout();
    }

    public function indexAction()
    {
        $baseUrl = Zend_Registry::get('config')->site->base_url;
        $this->view->baseUrl = $baseUrl;
    }

    public function getsessiondataAction() {
        
        $userID = (int) $this->getRequest()->getParam('user_id');

        $result = Application_Model_DbTable_UserPositions::getLastUserRoute( $userID );
        
        header('Content-type: application/json');
        echo json_encode( $result );die;
    }
    
    public function getlistofusersAction() {
        $response = Application_Model_DbTable_Users::getUsersInfo();
        
        header('Content-type: application/json');
        echo json_encode($response);die;
    }

}

