<?php

class AuthController extends Zend_Controller_Action
{

    public function init()
    {
//        $this->_helper->viewRenderer->setNoRender(true);
//        $this->_helper->layout->disableLayout();
    }

    public function indexAction()
    { 
        header('Content-type: application/json');
        $loginResults = array(
            'action'    => 'login'
        );
        try {
            $params = $this->getRequest()->getParams();
            
            $login = isset($params['login']) ? $params['login'] : '';
            $pass = isset($params['password']) ? $params['password'] : '';   

            if( !$login ) {
                throw new Exception('Wrong login');
            }
            if( !$pass) {
                throw new Exception('Wrong password');
            }

            $users = new Application_Model_DbTable_Users();

            $loginResults = $users->login($login, $pass);

            echo json_encode($loginResults);
            die;
        } catch (Exception $e) {
            $loginResults['success'] = false;
            $loginResults['message'] = $e->getMessage(); 
            
            echo json_encode($loginResults);
            die;
        }
        
    }

}

